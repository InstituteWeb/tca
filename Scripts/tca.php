<?php
namespace InstituteWeb\Mana\Scripts;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

if (file_exists(__DIR__ . '/../vendor/autoload.php')) {
    require_once(__DIR__ . '/../vendor/autoload.php');
} else if (file_exists(__DIR__ . '/../../../../vendor/autoload.php')) {
    require_once(__DIR__ . '/../../../../vendor/autoload.php');
}

$application = new \Symfony\Component\Console\Application(implode(PHP_EOL, [
    '+-------------------------------------------------------------------+',
    '|                                                                   |',
    '|  Institute Web // EXT:tca                                         |',
    '|  Table Configuration Array (TCA) Tools                            |',
    '|                                                         1.0.0@dev |',
    '+-------------------------------------------------------------------+',
]));


$application->add(new \InstituteWeb\Tca\Command\Debug());
$application->run();
