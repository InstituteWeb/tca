<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'TCA - Abstraction',
    'description' => 'TCA abstraction.',
    'category' => 'Misc',
    'author' => 'Armin Ruediger Vieweg',
    'author_email' => 'armin@v.ieweg.de',
    'author_company' => '',
    'shy' => '',
    'dependencies' => '',
    'conflicts' => '',
    'priority' => '',
    'module' => '',
    'state' => 'alpha',
    'internal' => '',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '0.1.0-dev',
    'constraints' => array(
        'depends' => array(
            'typo3' => '7.6.0-8.99.99'
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
    'suggests' => array(),
);
