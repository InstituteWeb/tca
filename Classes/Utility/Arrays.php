<?php
namespace InstituteWeb\Tca\Utility;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Arrays Utility
 *
 * @package InstituteWeb\Tca
 */
class Arrays
{
    /**
     * Returns a value by given path
     *
     * Example
     * - array:
     * array(
     *   'foo' => array(
     *     'bar' => array(
     *       'baz' => 42
     *     )
     *   )
     * );
     * - path: foo.bar.baz
     * - return: 42
     *
     * If a path segments contains a delimiter character, the path segment
     * must be enclosed by " (double quote), see unit tests for details
     *
     * @param array $array Input array
     * @param string $path Path within the array
     * @param string $delimiter Defined path delimiter, default .
     * @return mixed
     * @see \TYPO3\CMS\Core\Utility\ArrayUtility::getValueByPath
     */
    public static function getValueByPath(array $array, $path, $delimiter = '.')
    {
        if (empty($path)) {
            throw new \RuntimeException('Path must not be empty', 1341397767);
        }
        // Extract parts of the path
        $path = str_getcsv($path, $delimiter);
        // Loop through each part and extract its value
        $value = $array;
        foreach ($path as $segment) {
            if (array_key_exists($segment, $value)) {
                // Replace current value with child
                $value = $value[$segment];
            } else {
                // Fail if key does not exist
                return null;
            }
        }
        return $value;
    }

    /**
     * Modifies or sets a new value in an array by given path
     *
     * Example:
     * - array:
     * array(
     *   'foo' => array(
     *     'bar' => 42,
     *   ),
     * );
     * - path: foo.bar
     * - value: 23
     * - return:
     * array(
     *   'foo' => array(
     *     'bar' => 23,
     *   ),
     * );
     *
     * @param array $array Input array to manipulate
     * @param string $path Path in array to search for
     * @param mixed $value Value to set at path location in array
     * @param string $delimiter Path delimiter
     * @return void
     * @throws \RuntimeException
     * @see \TYPO3\CMS\Core\Utility\ArrayUtility::setValueByPath
     */
    public static function setValueByPath(array &$array, $path, $value, $delimiter = '.')
    {
        if (empty($path)) {
            throw new \RuntimeException('Path must not be empty', 1341406194);
        }
        if (!is_string($path)) {
            throw new \RuntimeException('Path must be a string', 1341406402);
        }
        // Extract parts of the path
        $path = str_getcsv($path, $delimiter);
        // Point to the root of the array
        $pointer = &$array;
        // Find path in given array
        foreach ($path as $segment) {
            // Fail if the part is empty
            if (empty($segment)) {
                throw new \RuntimeException('Invalid path segment specified', 1341406846);
            }
            // Create cell if it doesn't exist
            if (!array_key_exists($segment, $pointer)) {
                $pointer[$segment] = array();
            }
            // Set pointer to new cell
            $pointer = &$pointer[$segment];
        }
        // Set value of target cell
        $pointer = $value;
    }
}
