<?php
namespace InstituteWeb\Tca\Command;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Status Entities Command
 * Get status of configured entities
 *
 * @package InstituteWeb\Iwm
 */
class Debug extends \InstituteWeb\Tca\Command\AbstractCommand
{
    /**
     * Configure this command
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('debug')
            ->setDescription('Debug tca php configuration file')

            ->addArgument(
                'path',
                InputArgument::OPTIONAL,
                'Path to check.',
                getcwd()
            )
        ;
    }

    /**
     * Executes this command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initIO($input, $output);

        $path = realpath($input->getArgument('path'));
        if (file_exists($path) && is_file($path)) {
            $test = function() use ($path) {
                require ($path);

                foreach (get_defined_vars() as $variable => $value) {
                    /** @var $value \InstituteWeb\Tca\Structure\Table */
                    if ($value instanceof \InstituteWeb\Tca\Structure\Table) {
                        $this->output->outputLine($value->getName() . ' (stored in $' . $variable . ')');
                        $this->output->outputLine(print_r($value->toArray(), true));
                    }
                }
            };
            $test();
        }
    }
}
