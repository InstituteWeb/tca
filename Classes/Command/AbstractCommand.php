<?php
namespace InstituteWeb\Tca\Command;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Abstract Command
 *
 * @package InstituteWeb\Tca
 */
abstract class AbstractCommand extends \Symfony\Component\Console\Command\Command
{
    /**
     * Shortcut for DIRECTORY_SEPARATOR
     */
    const DS = DIRECTORY_SEPARATOR;

    /**
     * @var \InstituteWeb\Tca\Utility\ConsoleOutput
     */
    protected $output;

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var array
     */
    protected static $phpConfigurationArrays = [];

    /**
     * EnvironmentCommand constructor.
     *
     * @param string|null $name
     */
    public function __construct($name = null)
    {
        parent::__construct($name);
    }

    /**
     * Initializes $this->input and $this->output
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function initIO(InputInterface $input, OutputInterface $output)
    {
        error_reporting(E_ALL ^ E_NOTICE);
        $this->input = $input;
        $this->output = new Utility\ConsoleOutput($input, $output);
    }
}
