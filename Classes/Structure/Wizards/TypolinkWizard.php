<?php
namespace InstituteWeb\Tca\Structure\Wizards;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TypolinkWizard class
 *
 * @package InstituteWeb\Tca
 */
class TypolinkWizard extends AbstractWizard
{
    /**
     * @var array
     */
    protected $config = [
        'type' => 'popup',
        'title' => '',
        'module' => [
            'name' => 'wizard_element_browser',
            'urlParameters' => [
                'mode' => 'wizard'
            ]
        ],
        'icon' => 'link_popup.gif',
        'script' => 'browse_links.php?mode=wizard',
        'params' => [
            'blindLinkOptions' => 'page,file,folder,url,spec'
        ],
        'JSopenParams' => 'height=500,width=500,status=0,menubar=0,scrollbars=1'
    ];

    /**
     * @var array
     */
    protected $parentConfig = [
        'softref' => 'typolink,typolink_tag,images,url'
    ];

    /**
     * TypolinkWizard constructor.
     * @param string $key unique identifier of this wizard
     * @param string $blindLinkOptions page,file,folder,url,spec
     */
    public function __construct($key, $blindLinkOptions = 'page,file,folder,url,spec')
    {
        parent::__construct($key);
        Arrays::setValueByPath(
            $this->config,
            'params.blindLinkOptions',
            $blindLinkOptions
        );
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        Arrays::setValueByPath($this->config, 'config.title', (string) $title);
        return $this;
    }

    /**
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon)
    {
        Arrays::setValueByPath($this->config, 'config.icon', (string) $icon);
        return $this;
    }

    /**
     * @param bool $page Internal pages can be selected
     * @param bool $file Files can be selected
     * @param bool $folder Folders (from filelist) can be select
     * @param bool $url External url can be given
     * @param bool $spec Special
     * @return $this;
     */
    public function setBlindLinkOptions($page = true, $file = true, $folder = true, $url = true, $spec = true)
    {
        $enabledOptions = [];
        if ($page) {
            $enabledOptions[] = 'page';
        }
        if ($file) {
            $enabledOptions[] = 'file';
        }
        if ($folder) {
            $enabledOptions[] = 'folder';
        }
        if ($url) {
            $enabledOptions[] = 'url';
        }
        if ($spec) {
            $enabledOptions[] = 'spec';
        }
        Arrays::setValueByPath(
            $this->config,
            'params.blindLinkOptions',
            implode(',', array_filter($enabledOptions)),
            '.'
        );
        return $this;
    }

    /**
     * @param string $jsOpenParams
     * @return $this
     */
    public function setJsOpenParams($jsOpenParams)
    {
        Arrays::setValueByPath($this->config, 'config.JSopenParams', (string) $jsOpenParams);
        return $this;
    }

    /**
     * @param string $softref
     * @return $this
     */
    public function setSoftref($softref)
    {
        $this->parentConfig['softref'] = (string) $softref;
        return $this;
    }
}
