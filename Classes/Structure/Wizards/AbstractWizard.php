<?php
namespace InstituteWeb\Tca\Structure\Wizards;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * AbstractWizard class
 *
 * @package InstituteWeb\Tca
 */
abstract class AbstractWizard
{
    use \InstituteWeb\Tca\Structure\Traits\ContainsConfigurations;

    /**
     * @var string key of this wizard
     */
    protected $key;

    /**
     * @var null|array
     */
    protected $parentConfig;

    /**
     * AbstractWizard constructors
     *
     * @param string $key
     * @return AbstractWizard
     */
    public function __construct($key)
    {
        $this->key = (string) $key;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return array
     */
    public function getParentConfig()
    {
        return is_array($this->parentConfig) ? $this->parentConfig : [];
    }

    /**
     * hasParentConfig
     *
     * @return bool
     */
    public function hasParentConfig()
    {
        return !empty($this->parentConfig);
    }
}
