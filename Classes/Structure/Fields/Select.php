<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Structure\Traits;
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TCA field type Select
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/Select/Index.html
 */
class Select extends AbstractField
{
    use Traits\FieldHasWizards;

    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'select']];

    /**
     * Contains the elements for the selector box unless the property "foreign_table" or "special" has been set in which
     * case automated values are set in addition to any values listed in this array.
     *
     * @param array $items
     * @return Select
     */
    public function setItems(array $items)
    {
        Arrays::setValueByPath($this->config, 'config.items', $items);
        return $this;
    }

    /**
     * PHP function which is called to fill / manipulate the array with elements.
     *
     * @param string $itemsProcFunc function reference
     * @return Select
     */
    public function setItemsProcFunc($itemsProcFunc)
    {
        Arrays::setValueByPath($this->config, 'config.itemsProcFunc', (string) $itemsProcFunc);
        return $this;
    }

    /**
     * The number of rows in which to position the icons for the selector box. Default is to render as many columns as icons.
     *
     * @param int $seliconCols
     * @return Select
     */
    public function setSeliconCols($seliconCols)
    {
        Arrays::setValueByPath($this->config, 'config.selicon_cols', (int) $seliconCols);
        return $this;
    }

    /**
     * Controls the rendering of the icons after the select even when icons for the <select>'s <option> tags were supplied.
     *
     * @param bool $enable
     * @return Select
     */
    public function setShowIconTable($enable)
    {
        Arrays::setValueByPath($this->config, 'config.showIconTable', (bool) $enable);
        return $this;
    }

    /**
     * The item-array will be filled with records from the table defined here.
     * The table must be configured in $GLOBALS['TCA'].
     *
     * @param string $foreignTable
     * @return Select
     */
    public function setForeignTable($foreignTable)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_table', (string) $foreignTable);
        return $this;
    }

    /**
     * The items from foreign_table are selected with this WHERE-clause.
     * The table is joined with the "pages"-table and items are selected only from pages where the user has read access!
     * (Not checking DB mount limitations!)
     *
     * @param string $foreignTableWhere
     * @return Select
     */
    public function setForeignTableWhere($foreignTableWhere)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_table_where', (string) $foreignTableWhere);
        return $this;
    }

    /**
     * Label prefix to the title of the records from the foreign-table.
     *
     * @param string $foreignTablePrefix string or LLL reference
     * @return Select
     */
    public function setForeignTablePrefix($foreignTablePrefix)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_table_prefix', (string) $foreignTablePrefix);
        return $this;
    }

    /**
     * Specifying a folder from where files are added to the item array.
     *
     * @param mixed $fileFolder
     * @return Select
     */
    public function setFileFolder($fileFolder)
    {
        Arrays::setValueByPath($this->config, 'config.fileFolder', (string) $fileFolder);
        return $this;
    }

    /**
     * List of extensions to select. If blank, all files are selected. Specify list in lowercase.
     *
     * @param string $fileFolderExtList
     * @return Select
     */
    public function setFileFolderExtList($fileFolderExtList)
    {
        Arrays::setValueByPath($this->config, 'config.fileFolder_extList', (string) $fileFolderExtList);
        return $this;
    }

    /**
     * Depth of directory recursions. Default is 99. Specify in range from 0-99.
     * 0 (zero) means no recursion into subdirectories.
     *
     * @param mixed $fileFolderRecursions
     * @return Select
     */
    public function setFileFolderRecursions($fileFolderRecursions)
    {
        Arrays::setValueByPath($this->config, 'config.fileFolder_recursions', (int) $fileFolderRecursions);
        return $this;
    }

    /**
     * If "foreign_table" is enabled: If set, then values which are not integer ids will be allowed. May be needed if
     * you use itemsProcFunc or just enter additional items in the items array to produce some string-value elements for
     * the list.
     *
     * Notice: If you mix non-database relations with database relations like this, DO NOT use integers for values
     * and DO NOT use "_" (underscore) in values either!
     *
     * @param mixed $enable
     * @return Select
     */
    public function setAllowNonIdValues($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.allowNonIdValues', (bool) $enable);
        return $this;
    }

    /**
     * Set it to the exact same value as foreign_table if you don't want values to be remapped on copy.
     *
     * @param string $dontRemapTablesOnCopy
     * @return Select
     */
    public function setDontRemapTablesOnCopy($dontRemapTablesOnCopy)
    {
        Arrays::setValueByPath($this->config, 'config.dontRemapTablesOnCopy', (string) $dontRemapTablesOnCopy);
        return $this;
    }

    /**
     * If set, the foreign_table_where will be ignored and a "pid=0" will be added to the query to select only records
     * from root level of the page tree.
     *
     * @param bool $enable
     * @return Select
     */
    public function setRootLevel($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.rootLevel', (bool) $enable);
        return $this;
    }

    /**
     * Means that the relation to the records of foreign_table is done with a M-M relation with a third "join" table.
     *
     * @param string $mm table name
     * @return Select
     */
    public function setMm($mm)
    {
        Arrays::setValueByPath($this->config, 'config.MM', (string) $mm);
        return $this;
    }

    /**
     * If you want to make a MM relation editable from the foreign side (bidirectional) of the relation as well,
     * you need to set MM_opposite_field on the foreign side to the field name on the local side.
     *
     * @param string $mmOppositeField field name
     * @return Select
     */
    public function setMmOppositeField($mmOppositeField)
    {
        Arrays::setValueByPath($this->config, 'config.MM_opposite_field', (string) $mmOppositeField);
        return $this;
    }

    /**
     *
     *
     * @param array $mmMatchFields
     * @return Select
     */
    public function setMmMatchFields(array $mmMatchFields)
    {
        Arrays::setValueByPath($this->config, 'config.MM_match_fields', $mmMatchFields);
        return $this;
    }

    /**
     *
     *
     * @param array $mmOppositeUsage
     * @return Select
     */
    public function setMmOppositeUsage(array $mmOppositeUsage)
    {
        Arrays::setValueByPath($this->config, 'config.MM_oppositeUsage', $mmOppositeUsage);
        return $this;
    }

    /**
     * Array of field=>value pairs to insert when writing new MM relations
     *
     * @param array $mmInsertFields
     * @return Select
     */
    public function setMmInsertFields(array $mmInsertFields)
    {
        Arrays::setValueByPath($this->config, 'config.MM_insert_fields', $mmInsertFields);
        return $this;
    }

    /**
     * Additional where clause used when reading MM relations.
     *
     * @param string $mmTableWhere sql where
     * @return Select
     */
    public function setMmTableWhere($mmTableWhere)
    {
        Arrays::setValueByPath($this->config, 'config.MM_table_where', (string) $mmTableWhere);
        return $this;
    }

    /**
     * If the "multiple" feature is used with MM relations you MUST set this value to true and include a UID field!
     * Otherwise sorting and removing relations will be buggy.
     *
     * @param bool $enable
     * @return Select
     */
    public function setMmHasUidField($enable)
    {
        Arrays::setValueByPath($this->config, 'config.MM_hasUidField', (bool) $enable);
        return $this;
    }

    /**
     * This configures the selector box to fetch content from some predefined internal source.
     *
     * These are the possibilities:
     *
     * * tables - the list of TCA tables is added to the selector (excluding "adminOnly" tables).
     * * agetypes - all "doktype"-values for the "pages" table are added.
     * * exclude - the list of "excludeFields" as found in $TCA is added.
     * * modListGroup - module-lists added for groups.
     * * modListUser - module-lists added for users.
     * * explicitValues – List values that require explicit permissions to be allowed or denied. (See authMode).
     * * languages – List system languages ("sys_language" records from page tree root + Default language)
     * * custom – Custom values set by backend modules (see TYPO3_CONF_VARS[BE][customPermOptions])
     *
     * As you might have guessed these options are used for backend user management and pretty worthless for most other
     * purposes.
     *
     * @param string $special keyword
     * @return Select
     */
    public function setSpecial($special)
    {
        Arrays::setValueByPath($this->config, 'config.special', (string) $special);
        return $this;
    }

    /**
     * Height of the selector box in TCEforms.
     *
     * @param int $size
     * @return Select
     */
    public function setSize($size)
    {
        Arrays::setValueByPath($this->config, 'config.size', (int) $size);
        return $this;
    }

    /**
     * If set, then the height of multiple-item selector boxes (maxitems > 1) will automatically be adjusted to the
     * number of selected elements, however never less than "size" and never larger than the integer value of
     * "autoSizeMax" itself (takes precedence over "size"). So "autoSizeMax" is the maximum height the selector can
     * ever reach.
     *
     * @param int $autoSizeMax
     * @return Select
     */
    public function setAutoSizeMax($autoSizeMax)
    {
        Arrays::setValueByPath($this->config, 'config.autoSizeMax', (int) $autoSizeMax);
        return $this;
    }

    /**
     * If set, this will override the default style of the selector box with selected items (which is "width:200px").
     * Applies for when maxitems is > 1
     *
     * @param string $selectedListStyle
     * @return Select
     */
    public function setSelectedListStyle($selectedListStyle)
    {
        Arrays::setValueByPath($this->config, 'config.selectedListStyle', (string) $selectedListStyle);
        return $this;
    }

    /**
     * If set, this will override the default style of the selector box with available items to select (which is "width:200px").
     * Applies for when maxitems is > 1
     *
     * @param string $itemListStyle
     * @return Select
     */
    public function setItemListStyle($itemListStyle)
    {
        Arrays::setValueByPath($this->config, 'config.itemListStyle', (string) $itemListStyle);
        return $this;
    }

    /**
     * This setting specifies how the select field should be displayed.
     * Available options are: selectSingle, selectSingleBox, selectCheckbox, selectMultipleSideBySide, selectTree
     *
     * @param string $renderType
     * @return Select
     */
    public function setRenderType($renderType)
    {
        Arrays::setValueByPath($this->config, 'config.renderType', (string) $renderType);
        return $this;
    }

    /**
     * Configuration if the renderType is set to "selectTree".
     * Either childrenField or parentField has to be set - childrenField takes precedence.
     *
     * @param array $treeConfig
     * @return Select
     */
    public function setTreeConfig(array $treeConfig)
    {
        Arrays::setValueByPath($this->config, 'config.treeConfig', $treeConfig);
        return $this;
    }

    /**
     * Allows the same item more than once in a list.
     * If used with bidirectional MM relations it must be set for both the native and foreign field configuration.
     * Also, with MM relations in general you must use a UID field in the join table, see description for "MM"
     *
     * @param bool $enable
     * @return Select
     */
    public function setMultiple($enable)
    {
        Arrays::setValueByPath($this->config, 'config.multiple', (bool) $enable);
        return $this;
    }

    /**
     * Maximum number of items in the selector box. (Default = 1)
     *
     * @param int $maxitems
     * @return Select
     */
    public function setMaxitems($maxitems)
    {
        Arrays::setValueByPath($this->config, 'config.maxitems', (int) $maxitems);
        return $this;
    }

    /**
     * Minimum number of items in the selector box. (Default = 0)
     *
     * @param int $minitems
     * @return Select
     */
    public function setMinitems($minitems)
    {
        Arrays::setValueByPath($this->config, 'config.minitems', (int) $minitems);
        return $this;
    }

    /**
     * If set, then no element is inserted if the current value does not match any of the existing elements.
     * A corresponding options is also found in Page TSconfig.
     *
     * @param bool $enable
     * @return Select
     */
    public function setDisableNoMatchingValueElement($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.attr', (bool) $enable);
        return $this;
    }

    /**
     * If set, a textual field is shown above the available items in which one can type words to filter the list of
     * available items. Applies only when maxitems is > 1.
     *
     * @param bool $enable
     * @return Select
     */
    public function setEnableMultiSelectFilterTextfield($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.enableMultiSelectFilterTextfield', (string) $enable);
        return $this;
    }

    /**
     * Contains predefined elements for the filter field enabled by enableMultiSelectFilterTextfield.
     * On selecting a item, the list of available items gets automatically filtered.
     *
     * @param array $multiSelectFilterItems
     * @return Select
     */
    public function setMultiSelectFilterItems(array $multiSelectFilterItems)
    {
        Arrays::setValueByPath($this->config, 'config.multiSelectFilterItems', $multiSelectFilterItems);
        return $this;
    }

    /**
     * Authorization mode for the selector box.
     * Keywords are: 'explicitAllow', 'explicitDeny', 'individual'
     *
     * @param mixed $authMode
     * @return Select
     */
    public function setAuthMode($authMode)
    {
        Arrays::setValueByPath($this->config, 'config.authMode', (string) $authMode);
        return $this;
    }

    /**
     * Various additional enforcing options for authMode.
     * Keywords are: 'strict'
     *
     * @param string $authModeEnforce
     * @return Select
     */
    public function setAuthModeEnforce($authModeEnforce)
    {
        Arrays::setValueByPath($this->config, 'config.authMode_enforce', (string) $authModeEnforce);
        return $this;
    }

    /**
     * List of keys that exclude any other keys in a select box where multiple items could be selected.
     * "Show at any login" of "fe_groups" (tables "pages" and "tt_content") is an example where such a configuration is
     * used.
     *
     * @param string $exclusiveKeys
     * @return Select
     */
    public function setExclusiveKeys($exclusiveKeys)
    {
        Arrays::setValueByPath($this->config, 'config.exclusiveKeys', (string) $exclusiveKeys);
        return $this;
    }

    /**
     * Defines whether referenced records should be localized when the current record gets localized
     * (mostly used in Inline Relational Record Editing)
     *
     * @param bool $enable
     * @return Select
     */
    public function setLocalizeReferencesAtParentLocalization($enable)
    {
        Arrays::setValueByPath($this->config, 'config.localizeReferencesAtParentLocalization', (bool) $enable);
        return $this;
    }
}
