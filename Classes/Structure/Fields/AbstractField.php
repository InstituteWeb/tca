<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * Abstract class Column
 *
 * @package InstituteWeb\Tca
 */
abstract class AbstractField
{
    use \InstituteWeb\Tca\Structure\Traits\ContainsConfigurations;

    /**
     * @var string Key of this column (also field name in database, e.g. 'my_cool_field')
     */
    protected $key;

    /**
     * @var array
     */
    protected $config = ['exclude' => false, 'config' => []];


    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return AbstractField
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @param string $label string or LLL-Reference (eg. 'LLL:EXT:example/Resources/Private/Langauges/locallang.xlf:key')
     * @return AbstractField
     */
    public function setLabel($label)
    {
        Arrays::setValueByPath($this->config, 'label', $label);
        return $this;
    }

    /**
     * @param array|string $displayCond
     * @return AbstractField
     */
    public function setDisplayCond($displayCond)
    {
        Arrays::setValueByPath($this->config, 'displayCond', $displayCond);
        return $this;
    }

    /**
     * @param string $defaultExtras
     * @return AbstractField
     */
    public function setDefaultExtras($defaultExtras)
    {
        Arrays::setValueByPath($this->config, 'defaultExtras', $defaultExtras);
        return $this;
    }

    /**
     * @param boolean $exclude
     * @return AbstractField
     */
    public function setExclude($exclude)
    {
        Arrays::setValueByPath($this->config, 'exclude', (bool) $exclude);
        return $this;
    }

    /**
     * @param string $l10n_mode
     * @return AbstractField
     */
    public function setL10nMode($l10n_mode)
    {
        Arrays::setValueByPath($this->config, 'config.l10n_mode', $l10n_mode);
        return $this;
    }

    /**
     * @param string $l10n_display
     * @return AbstractField
     */
    public function setL10nDisplay($l10n_display)
    {
        Arrays::setValueByPath($this->config, 'config.l10n_display', $l10n_display);
        return $this;
    }

    /**
     * @param string $l10n_cat
     * @return AbstractField
     */
    public function setL10nCat($l10n_cat)
    {
        Arrays::setValueByPath($this->config, 'config.l10n_cat', $l10n_cat);
        return $this;
    }

    /**
     * @param string $dbType
     * @return AbstractField
     */
    public function setDbType($dbType)
    {
        Arrays::setValueByPath($this->config, 'config.dbType', $dbType);
        return $this;
    }

    /**
     * @param mixed $default
     * @return AbstractField
     */
    public function setDefault($default)
    {
        Arrays::setValueByPath($this->config, 'config.default', $default);
        return $this;
    }

    /**
     * Renders the form in a way that the user can see the values but cannot edit them. The rendering is as similar as
     * possible to the normal rendering but may differ in layout and size.
     *
     * @param bool $enable
     * @return AbstractField
     */
    public function setReadOnly($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.readOnly', (bool) $enable);
        return $this;
    }

    /**
     * @param array $search
     * @return AbstractField
     */
    public function setSearch(array $search)
    {
        Arrays::setValueByPath($this->config, 'config.search', $search);
        return $this;
    }

    /**
     * Used to attach "soft reference parsers". See under "Additional TCA features" for information about softref keys.
     * The syntax for this value is key1,key2[parameter1;parameter2;...],...
     *
     * @param string $softRef
     * @return AbstractField
     */
    public function setSoftRef($softRef)
    {
        Arrays::setValueByPath($this->config, 'config.softref', (string) $softRef);
        return $this;
    }
}
