<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * TCA field type Passthrough
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/Passthrough/Index.html
 */
class Passthrough extends AbstractField
{
    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'passthrough']];
}
