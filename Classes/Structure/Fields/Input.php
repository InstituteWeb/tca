<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Structure\Traits;
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TCA field type Input
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/Input/Index.html
 */
class Input extends AbstractField
{
    use Traits\FieldHasWizards;

    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'input', 'size' => 30]];


    /**
     * Input field constructor.
     *
     * @param string $key
     * @param string $label
     * @param string|null $eval
     * @param string|null $default
     * @param int $size
     */
    public function __construct($key, $label, $eval = null, $default = null, $size = 30)
    {
        $this->setKey($key);
        Arrays::setValueByPath($this->config, 'label', (string) $label);

        if (!is_null($eval)) {
            Arrays::setValueByPath($this->config, 'config.eval', (string) $eval);
        }
        if (!is_null($default)) {
            Arrays::setValueByPath($this->config, 'config.default', (string) $default);
        }
        if (is_int($size)) {
            Arrays::setValueByPath($this->config, 'config.size', (int) $size);
        }
    }

    /**
     * @param bool $enable
     * @return Input
     */
    public function setAutocomplete($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.autocomplete', (bool) $enable);
        return $this;
    }

    /**
     * @param string $eval Evaluations (e.g. 'trim,required')
     * @return Input
     */
    public function setEval($eval)
    {
        Arrays::setValueByPath($this->config, 'config.eval', $eval);
        return $this;
    }

    /**
     * @param string $format
     * @return Input
     */
    public function setFormat($format)
    {
        Arrays::setValueByPath($this->config, 'config.format', $format);
        return $this;
    }

    /**
     * @param string $isIn
     * @return Input
     */
    public function setIsIn($isIn)
    {
        Arrays::setValueByPath($this->config, 'config.is_in', (string) $isIn);
        return $this;
    }

    /**
     * @param int $max
     * @return Input
     */
    public function max($max)
    {
        Arrays::setValueByPath($this->config, 'config.max', (int) $max);
        return $this;
    }

    /**
     * @param string $mode
     * @return Input
     */
    public function mode($mode)
    {
        Arrays::setValueByPath($this->config, 'config.mode', (string) $mode);
        return $this;
    }

    /**
     * @param string $placeholder
     * @return Input
     */
    public function setPlaceholder($placeholder)
    {
        Arrays::setValueByPath($this->config, 'config.placeholder', (string) $placeholder);
        return $this;
    }

    /**
     * @param array $range
     * @return Input
     */
    public function setRange(array $range)
    {
        Arrays::setValueByPath($this->config, 'config.range', $range);
        return $this;
    }

    /**
     * @param int $size
     * @return Input
     */
    public function setSize($size)
    {
        Arrays::setValueByPath($this->config, 'config.size', (int) $size);
        return $this;
    }
}
