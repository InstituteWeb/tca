<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TCA field type None
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/None/Index.html
 */
class None extends AbstractField
{
    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'none']];


    /**
     * The value of a none-type fields is normally displayed as is. It is however possible to format it using this
     * property.
     *
     * @param string $format keyword
     * @return None
     */
    public function setFormat($format)
    {
        Arrays::setValueByPath($this->config, 'config.format', $format);
        return $this;
    }

    /**
     * The keywords have sub-properties. Sub-properties are called with the format.
     *
     * @param array $formatArray
     * @return None
     */
    public function setFormatArray(array $formatArray)
    {
        Arrays::setValueByPath($this->config, 'config.format.', $formatArray);
        return $this;
    }

    /**
     * If set, then content from the field is directly outputted in the <div> section. Otherwise the content will be
     * passed through htmlspecialchars() and possibly nl2br() if there is configuration for rows.
     *
     * @param bool $enable
     * @return None
     */
    public function setPassContent($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.pass_content', (bool) $enable);
        return $this;
    }

    /**
     * If this value is greater than 1 the display of the non-editable content will be shown in a <div> area trying to
     * simulate the rows/columns known from a text-type element.
     *
     * @param int $rows
     * @return None
     */
    public function setRows($rows)
    {
        Arrays::setValueByPath($this->config, 'config.rows', (int) $rows);
        return $this;
    }

    /**
     * See rows and size.
     *
     * @param int $cols
     * @return None
     */
    public function setCols($cols)
    {
        Arrays::setValueByPath($this->config, 'config.cols', (int) $cols);
        return $this;
    }

    /**
     * If this is set the <div> element will not automatically try to fit the content length but rather respect the
     * size selected by the value of the rows key.
     *
     * @param bool $enable
     * @return None
     */
    public function setFixedRows($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.fixedRows', (bool) $enable);
        return $this;
    }

    /**
     * If rows is less than one, the cols value is used to set the width of the field and if cols is not found, then
     * size is used to set the width. The measurements corresponds to those of input and text type fields.
     *
     * @param int $size
     * @return None
     */
    public function setSize($size)
    {
        Arrays::setValueByPath($this->config, 'config.size', (int) $size);
        return $this;
    }
}
