<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TCA field type Flex
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/Flex/Index.html
 */
class Flex extends AbstractField
{
    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'flex']];


    /**
     * Data Structure(s) defined in an array.
     *
     * @param array $ds
     * @return Flex
     */
    public function setDs(array $ds)
    {
        Arrays::setValueByPath($this->config, 'config.ds', $ds);
        return $this;
    }

    /**
     * Contains the value "[table]:[field name]" from which to fetch Data Structure XML. ds_pointerField is in this
     * case the pointer which should contain the uid of a record from that table.
     *
     * @param string $dsTableField
     * @return Flex
     */
    public function setDsTableField($dsTableField)
    {
        Arrays::setValueByPath($this->config, 'config.ds_tableField', (string) $dsTableField);
        return $this;
    }

    /**
     * Field name(s) in the record which point to the field where the key for "ds" is found. Up to two field names can
     * be specified comma separated.
     *
     * @param string $dsPointerField
     * @return Flex
     */
    public function setDsPointerField($dsPointerField)
    {
        Arrays::setValueByPath($this->config, 'config.ds_pointerField', (string) $dsPointerField);
        return $this;
    }

    /**
     * Used to search for Data Structure recursively back in the table assuming that the table is a tree table.
     * This value points to the "pid" field. See "templavoila" for example - uses this for the Page Template.
     *
     * @param string $value
     * @return Flex
     */
    public function setDsPointerFieldSearchParent($value)
    {
        Arrays::setValueByPath($this->config, 'config.ds_pointerField_searchParent', (string) $value);
        return $this;
    }

    /**
     * Points to a field in the "rootline" which may contain a pointer to the "next-level" template.
     * See "templavoila" for example - uses this for the Page Template.
     *
     * @param string $value
     * @return Flex
     */
    public function setDsPointerFieldSearchParentSubField($value)
    {
        Arrays::setValueByPath($this->config, 'config.ds_pointerField_searchParent_subField', (string) $value);
        return $this;
    }
}
