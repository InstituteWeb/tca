<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TCA field type Check
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/Check/Index.html
 */
class Check extends \InstituteWeb\Tca\Structure\Fields\AbstractField
{
    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'check']];


    /**
     * Check field constructor
     *
     * @param string $key
     * @param string $label
     * @param array|null $items
     * @param int|string|null $cols Integer between 1-31 or string 'inline'
     */
    public function __construct($key, $label, array $items = null, $cols = null)
    {
        $this->setKey($key);
        Arrays::setValueByPath($this->config, 'label', (string) $label);

        if (!is_null($items)) {
            $this->setItems($items);
        }
        if (!is_null($cols)) {
            $this->setCols($cols);
        }
    }

    /**
     * @param array $items
     * @return Check
     */
    public function setItems(array $items)
    {
        Arrays::setValueByPath($this->config, 'config.items', $items);
        return $this;
    }

    /**
     * @param int|string $cols Integer between 1-31 or string 'inline'
     * @return Check
     */
    public function setCols($cols)
    {
        Arrays::setValueByPath($this->config, 'config.cols', $cols);
        return $this;
    }

    /**
     * If set, this field will show only if the RTE editor is enabled (which includes correct browser version
     * and user-rights altogether.)
     *
     * @param bool $enable
     * @return Check
     */
    public function setShowIfRTE($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.showIfRTE', (bool) $enable);
        return $this;
    }

    /**
     * PHP function which is called to fill / manipulate the array with elements.
     *
     * @param string $itemsProcFunc
     * @return Check
     */
    public function setItemsProcFunc($itemsProcFunc)
    {
        Arrays::setValueByPath($this->config, 'config.itemsProcFunc', (string) $itemsProcFunc);
        return $this;
    }

    /**
     * Configuration of field evaluation.
     *
     * @param string $eval list of keywords: 'maximumRecordsChecked', 'maximumRecordsCheckedInPid'
     * @return Check
     */
    public function setEval($eval)
    {
        Arrays::setValueByPath($this->config, 'config.eval', (string) $eval);
        return $this;
    }

    /**
     * Values for the eval rules.
     * The keys of the array must correspond to the keyword of the related evaluation rule. The value will generally a
     * number or whatever else is approppriate for the evaluation rule. For maximumRecordsChecked and
     * maximumRecordsCheckedInPid the value is expected to be an integer.
     *
     * @param array $validation
     * @return Check
     */
    public function setValidation(array $validation)
    {
        Arrays::setValueByPath($this->config, 'config.validation', $validation);
        return $this;
    }
}
