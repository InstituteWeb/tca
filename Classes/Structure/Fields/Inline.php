<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TCA field type Inline
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/Inline/Index.html
 */
class Inline extends AbstractField
{
    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'inline']];


    /**
     * [Must be set, there is no type "inline" without a foreign table] The table name of the child records is defined
     * here. The table must be configured in $TCA.
     *
     * @param string $foreignTable
     * @return Inline
     */
    public function setForeignTable($foreignTable)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_table', (string) $foreignTable);
        return $this;
    }

    /**
     * Has information about the appearance of child-records
     *
     * @param array $appearance
     * @return Inline
     * @TODO: Create a convinience class for the appearance options
     */
    public function setAppearance(array $appearance)
    {
        Arrays::setValueByPath($this->config, 'config.appearance', $appearance);
        return $this;
    }

    /**
     * Has information about the behavior of child-records, namely
     *
     * @param array $behaviour
     * @return Inline
     */
    public function setBehaviour(array $behaviour)
    {
        Arrays::setValueByPath($this->config, 'config.behaviour', $behaviour);
        return $this;
    }

    /**
     * Numerical array containing definitions of custom header controls for IRRE fields. This makes it possible to create
     * special controls by calling user-defined functions (userFuncs). Each item in the array item must be an array itself,
     * with at least on key "userFunc" pointing to the user function to call.
     *
     * @param array $customControls
     * @return Inline
     */
    public function setCustomControls(array $customControls)
    {
        Arrays::setValueByPath($this->config, 'config.customControls', $customControls);
        return $this;
    }

    /**
     * The foreign_field is the field of the child record pointing to the parent record.
     * This defines where to store the uid of the parent record.
     *
     * @param string $foreignField
     * @return Inline
     */
    public function setForeignField($foreignField)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_field', (string) $foreignField);
        return $this;
    }

    /**
     * @param string $foreignLabel
     * @return Inline
     */
    public function setForeignLabel($foreignLabel)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_label', (string) $foreignLabel);
        return $this;
    }

    /**
     * A selector is used to show all possible child records that could be used to create a relation with the parent
     * record. It will be rendered as a multi-select-box. On clicking on an item inside the selector a new relation is
     * created.The foreign_selector points to a field of the foreign_table that is responsible for providing a
     * selector-box – this field on the foreign_table usually is of type select and also has a foreign_table defined.
     *
     * @param string $foreignSelector
     * @return Inline
     */
    public function setForeignSelector($foreignSelector)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_selector', $foreignSelector);
        return $this;
    }

    /**
     * TCA file configuration that overrides the configuration of the field defined in the foreign_selector property.
     *
     * @param array $foreignSelectorFieldTcaOverride
     * @return Inline
     */
    public function setForeignSelectorFieldTcaOverride(array $foreignSelectorFieldTcaOverride)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_selector_fieldTcaOverride', $foreignSelectorFieldTcaOverride);
        return $this;
    }

    /**
     * Define a field on the child record (or on the intermediate table) that stores the manual sorting information.
     * It is possible to have a different sorting, depending from which side of the relation we look at parent or child.
     * This property requires that the foreign_field approach is used.
     *
     * @param $foreignSortby
     * @return Inline
     */
    public function setForeignSortby($foreignSortby)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_sortby', (string) $foreignSortby);
        return $this;
    }

    /**
     * If a field name for foreign_sortby is defined, then this is ignored.
     * Otherwise this is used as the "ORDER BY" statement to sort the records in the table when listed.
     *
     * @param string $foreignDefaultSortby
     * @return Inline
     */
    public function setForeignDefaultSortby($foreignDefaultSortby)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_default_sortby', (string) $foreignDefaultSortby);
        return $this;
    }

    /**
     * The foreign_table_field is the field of the child record pointing to the parent record. This defines where to
     * store the table name of the parent record. On setting this configuration key together with foreign_field, the
     * child record knows what its parent record is – so the child record could also be used on other parent tables.
     *
     * This issue is also known as "weak entity".Do not confuse with foreign_table or foreign_field. It has its own behavior.
     *
     * @param string $foreignTableField
     * @return Inline
     */
    public function setForeignTableField($foreignTableField)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_table_field', $foreignTableField);
        return $this;
    }

    /**
     * This property makes it possible to set default values for the foreign records created via the inline relation.
     *
     * @param array $foreignRecordDefaults
     * @return Inline
     */
    public function setForeignRecordDefaults(array $foreignRecordDefaults)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_record_defaults', $foreignRecordDefaults);
        return $this;
    }

    /**
     * Field which must be unique for all children of a parent record.
     *
     * @param string $foreignUnique
     * @return Inline
     */
    public function setForeignUnique($foreignUnique)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_unique', (string) $foreignUnique);
        return $this;
    }

    /**
     * Possibility to define user functions to filter out child items. This is useful in special scenarios when used in
     * conjunction with a foreign_selector where only certain foreign records are allowed to be related to.
     *
     * @param array $filter
     * @return Inline
     */
    public function setFilter(array $filter)
    {
        Arrays::setValueByPath($this->config, 'config.filter', $filter);
        return $this;
    }

    /**
     * Means that the relation to the records of foreign_table is done with a M-M relation with a third "join" table.
     *
     * @param string $mm
     * @return Inline
     */
    public function setMM($mm)
    {
        Arrays::setValueByPath($this->config, 'config.MM', (string) $mm);
        return $this;
    }

    /**
     * Array of field-value pairs to both insert and match against when writing/reading IRRE relations.
     * Using the match fields, it is possible to re-use the same child table in more than one field of the parent table
     * by using a match field with different values for each of the use cases.
     *
     * @param array $foreignMatchFields
     * @return Inline
     */
    public function setForeignMatchFields(array $foreignMatchFields)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_match_fields', $foreignMatchFields);
        return $this;
    }

    /**
     * This can be used to control which fields of the child table are displayed. You can override the "showitem", etc.
     * settings of the child table here, by supplying an override for the "types" array of that table. For details on
     * how the types array is constructed, see the chapter "['types'][key] section" later in this manual.
     *
     * @param array $foreignTypes
     * @return Inline
     */
    public function setForeignTypes(array $foreignTypes)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_types', $foreignTypes);
        return $this;
    }

    /**
     * Height of the selector box in TCEforms.
     *
     * @param int $size
     * @return Inline
     */
    public function setSize($size)
    {
        Arrays::setValueByPath($this->config, 'config.size', (int) $size);
        return $this;
    }

    /**
     * If set, then the height of multiple-item selector boxes (maxitem > 1) will automatically be adjusted to the
     * number of selected elements, however never less than "size" and never larger than the integer value of "autoSizeMax"
     * itself (takes precedence over "size"). So "autoSizeMax" is the maximum height the selector can ever reach.
     *
     * @param int $autoSizeMax
     * @return Inline
     */
    public function setAutoSizeMax($autoSizeMax)
    {
        Arrays::setValueByPath($this->config, 'config.autoSizeMax', (int) $autoSizeMax);
        return $this;
    }

    /**
     * Maximum number of items in the selector box. Defaults to 100000. Note that this is different from types "select"
     * and "group" which default to 1.
     *
     * @param int $maxitems
     * @return Inline
     */
    public function setMaxitems($maxitems)
    {
        Arrays::setValueByPath($this->config, 'config.maxitems', (int) $maxitems);
        return $this;
    }

    /**
     * Minimum number of items in the selector box. (Default = 0)
     *
     * @param int $minitems
     * @return Inline
     */
    public function setMinitems($minitems)
    {
        Arrays::setValueByPath($this->config, 'config.minitems', (int) $minitems);
        return $this;
    }

    /**
     * This works like foreign_field, but in case of using bidirectional symmetric relations. symmetric_field defines
     * in which field on the foreign_table the uid of the "other" parent is stored.
     *
     * @param string $symmetricField
     * @return Inline
     */
    public function setSymmetricField($symmetricField)
    {
        Arrays::setValueByPath($this->config, 'config.symmetric_field', (string) $symmetricField);
        return $this;
    }

    /**
     * If set, it overrides the label set in $GLOBALS['TCA'][<foreign_table>]['ctrl']['label'] for the inline-view and
     * only if looking to a symmetric relation from the "other" side.
     *
     * @param string $symmetricLabel
     * @return Inline
     */
    public function setSymmetricLabel($symmetricLabel)
    {
        Arrays::setValueByPath($this->config, 'config.symmetric_label', (string) $symmetricLabel);
        return $this;
    }

    /**
     * This works like foreign_sortby, but in case of using bidirectional symmetric relations. Each side of a symmetric
     * relation could have its own sorting, so symmetric_sortby defines a field on the foreign_table where the sorting
     * of the "other" side is stored. This property requires that the foreign_field approach is used.
     *
     * @param string $symmetricSortby
     * @return Inline
     */
    public function setSymmetricSsortby($symmetricSortby)
    {
        Arrays::setValueByPath($this->config, 'config.symmetric_sortby', (string) $symmetricSortby);
        return $this;
    }
}
