<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TCA field type User
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/User/Index.html
 */
class User extends AbstractField
{
    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'user']];


    /**
     * Function or method reference.
     * If you want to call a function, just enter the function name. The function name must be prefixed "user_" or "tx_".
     * If you want to call a method in a class, enter "[classname]->[methodname]".
     *
     * @param string $userFunc
     * @return User
     */
    public function setUserFunc($userFunc)
    {
        Arrays::setValueByPath($this->config, 'config.userfunc', (string) $userFunc);
        return $this;
    }

    /**
     * Array that will be passed as is to the userFunc as the "parameters" key of the first argument received by the user function.
     *
     * @param array $parameters
     * @return User
     */
    public function setParameters(array $parameters)
    {
        Arrays::setValueByPath($this->config, 'config.parameters', $parameters);
        return $this;
    }


    /**
     * If set, then the output from the user function will not be wrapped in the usual table - you will have to do that yourself.
     *
     * @param bool $noTableWrapping
     * @return User
     */
    public function setNoTableWrapping($noTableWrapping)
    {
        Arrays::setValueByPath($this->config, 'config.noTableWrapping', (booL) $noTableWrapping);
        return $this;
    }
}
