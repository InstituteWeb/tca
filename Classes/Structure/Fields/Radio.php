<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TCA field type Radio
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/Radio/Index.html
 */
class Radio extends AbstractField
{
    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'radio']];


    /**
     * An array of the values which can be selected. Each entry is in itself an array where the first entry is the
     * title (string or LLL reference) and the second entry is the value of the radio button.
     *
     * @param array $items
     * @return Radio
     */
    public function setItems(array $items)
    {
        Arrays::setValueByPath($this->config, 'config.items', $items);
        return $this;
    }

    /**
     * PHP function which is called to fill or manipulate the array with elements.
     *
     * @param string $itemsProcFunc
     * @return Radio
     */
    public function setBlah($itemsProcFunc)
    {
        Arrays::setValueByPath($this->config, 'config.itemsProcFunc', (string) $itemsProcFunc);
        return $this;
    }
}
