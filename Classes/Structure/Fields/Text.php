<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Structure\Traits;
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TCA field type Text
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/Text/Index.html
 */
class Text extends AbstractField
{
    use Traits\FieldHasWizards;

    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'text']];


    /**
     * Text field constructor
     *
     * @param string $key
     * @param string $label
     * @param int|null $rows
     * @param int|null $cols
     * @param string|null $default
     * @param string $eval
     */
    public function __construct($key, $label, $rows = null, $cols = null, $default = null, $eval = null)
    {
        $this->setKey($key);
        Arrays::setValueByPath($this->config, 'label', (string) $label);

        if (!is_null($rows)) {
            Arrays::setValueByPath($this->config, 'config.rows', (int) $rows);
        }
        if (!is_null($cols)) {
            Arrays::setValueByPath($this->config, 'config.cols', (int) $cols);
        }
        if (!is_null($default)) {
            Arrays::setValueByPath($this->config, 'config.default', $default);
        }
        if (!is_null($eval)) {
            Arrays::setValueByPath($this->config, 'config.eval', (string) $eval);
        }
    }

    /**
     * Abstract value for the width of the <textarea> field. To set the textarea to the full width of the form area,
     * use the value 48. Default is 30.
     *
     * @param int $cols
     * @return Text
     */
    public function setCols($cols)
    {
        Arrays::setValueByPath($this->config, 'config.cols', (int) $cols);
        return $this;
    }

    /**
     * The number of rows in the textarea. May be corrected for harmonization between browsers. Will also automatically
     * be increased if the content in the field is found to be of a certain length, thus the field will automatically
     * fit the content. Default is 5. Max value is 20.
     *
     * @param int $rows
     * @return Text
     */
    public function setRows($rows)
    {
        Arrays::setValueByPath($this->config, 'config.rows', (int) $rows);
        return $this;
    }

    /**
     * Adds the HTML5 attribute "maxlength" to a textarea.
     *
     * @param int $max
     * @return Text
     */
    public function setMax($max)
    {
        Arrays::setValueByPath($this->config, 'config.max', (int) $max);
        return $this;
    }

    /**
     * Adds the HTML5 attribute "maxlength" to a textarea.
     *
     * @param string $wrap Keyword: 'virtual', 'off'
     * @return Text
     */
    public function setWrap($wrap)
    {
        Arrays::setValueByPath($this->config, 'config.wrap', (string) $wrap);
        return $this;
    }

    /**
     * Configuration of field evaluation.
     *
     * @param string $eval list of keywords: 'required', 'trim', 'tx_*'
     * @return Text
     */
    public function setEval($eval)
    {
        Arrays::setValueByPath($this->config, 'config.eval', (string) $eval);
        return $this;
    }

    /**
     * When an text-type field is set as read-only, it actually gets rendered as a none-type field.
     * This means that is is possible to use the format property of such field to format the value of the text-type field.
     *
     * @param string $format
     * @return String
     */
    public function setFormat($format)
    {
        Arrays::setValueByPath($this->config, 'config.format', $format);
        return $this;
    }

    /**
     * If a user-defined evaluation is used for the field (see eval key), then this value will be passed as argument
     * to the user-defined evaluation function.
     *
     * @param $isIn
     * @return Text
     */
    public function setIsIn($isIn)
    {
        Arrays::setValueByPath($this->config, 'config.is_in', $isIn);
        return $this;
    }

    /**
     * @param $placeholder
     * @return Text
     */
    public function setPlaceholder($placeholder)
    {
        Arrays::setValueByPath($this->config, 'config.placeholder', $placeholder);
        return $this;
    }

    /**
     * @param string $mode keywords
     * @return Text
     */
    public function setMode($mode)
    {
        Arrays::setValueByPath($this->config, 'config.mode', (string) $mode);
        return $this;
    }
}
