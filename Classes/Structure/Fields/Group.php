<?php
namespace InstituteWeb\Tca\Structure\Fields;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Structure\Traits;
use InstituteWeb\Tca\Utility\Arrays;

/**
 * TCA field type Group
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/Reference/Columns/Group/Index.html
 */
class Group extends AbstractField
{
    use Traits\FieldHasWizards;

    /**
     * @var array
     */
    protected $config = ['config' => ['type' => 'group']];


    /**
     * Configures the internal type of the "group" type of element.
     * There are four possible options to choose from:
     *
     * file:           this will create a field where files can be attached to the record (not recommeded to use)
     * file_reference: this will create a field where files can be referenced (not recommeded to use)
     * folder:         this will create a field where folders can be attached to the record
     * db:             this will create a field where database records can be attached as references
     *
     * @param string $internalType
     * @return Group
     */
    public function setInternalType($internalType)
    {
        Arrays::setValueByPath($this->config, 'config.internal_type', (string) $internalType);
        return $this;
    }

    /**
     * For the "file" internal type (Optional):
     * A lowercase comma list of file extensions that are permitted. E.g. 'jpg,gif,txt'. Also see 'disallowed'.
     *
     * For the "db" internal type (Required!):
     * A comma list of tables from $TCA.
     *
     * @param string $allowed list
     * @return Group
     */
    public function setAllowed($allowed)
    {
        Arrays::setValueByPath($this->config, 'config.allowed', $allowed);
        return $this;
    }

    /**
     * @param string $disallowed list
     * @return Group
     */
    public function setDisallowed($disallowed)
    {
        Arrays::setValueByPath($this->config, 'config.disallowed', (string) $disallowed);
        return $this;
    }

    /**
     * Define filters for item values.
     * [internal_type = *db* ONLY]
     *
     * @param array $filter
     * @return Group
     */
    public function setFilter(array $filter)
    {
        Arrays::setValueByPath($this->config, 'config.filter', $filter);
        return $this;
    }

    /**
     * This property does not really exist for group-type fields. It is needed as a workaround for an Extbase limitation.
     * It is used to resolve dependencies during Extbase persistence. It should hold the same values as property allowed.
     * Notice that only one table name is allowed here in contrast to the property allowed itself.
     *
     * @param string $foreignTable table name
     * @return Group
     */
    public function setForeignTable($foreignTable)
    {
        Arrays::setValueByPath($this->config, 'config.foreign_table', (string) $foreignTable);
        return $this;
    }

    /**
     * Defines MM relation table to use.
     * Means that the relation to the files/db is done with a M-M relation through a third "join" table.
     *
     * @param string $mm table name
     * @return Group
     */
    public function setMm($mm)
    {
        Arrays::setValueByPath($this->config, 'config.MM', (string) $mm);
        return $this;
    }

    /**
     * See MM_opposite_field property of select-type fields.
     *
     * @param array $mmOppositeFields
     * @return Group
     */
    public function setMmOppositeFields(array $mmOppositeFields)
    {
        Arrays::setValueByPath($this->config, 'config.MM_opposite_field', $mmOppositeFields);
        return $this;
    }

    /**
     * See MM_match_fields property of select-type fields.
     *
     * @param array $mmMatchFields
     * @return Group
     */
    public function setMmMatchFields(array $mmMatchFields)
    {
        Arrays::setValueByPath($this->config, 'config.MM_match_fields', $mmMatchFields);
        return $this;
    }

    /**
     * See MM_oppositeUsage property of select-type fields.
     *
     * @param array $mmOppositeUsage
     * @return Group
     */
    public function setMmOppositeUsage(array $mmOppositeUsage)
    {
        Arrays::setValueByPath($this->config, 'config.MM_oppositeUsage', $mmOppositeUsage);
        return $this;
    }

    /**
     * See MM_insert_fields property of select-type fields.
     *
     * @param array $mmInsertFields
     * @return Group
     */
    public function setMmInsertFields(array $mmInsertFields)
    {
        Arrays::setValueByPath($this->config, 'config.MM_insert_fields', $mmInsertFields);
        return $this;
    }

    /**
     * See MM_table_where property of select-type fields.
     *
     * @param string $mmTableWhere sql where
     * @return Group
     */
    public function setMmTableWhere($mmTableWhere)
    {
        Arrays::setValueByPath($this->config, 'config.MM_table_where', $mmTableWhere);
        return $this;
    }

    /**
     * See MM_hasUidField property of select-type fields.
     *
     * @param bool $enable
     * @return Group
     */
    public function setMmHasUidField($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.MM_hasUidField', $enable);
        return $this;
    }

    /**
     * Files: Maximum file size allowed in KB
     * [internal_type = *file* ONLY]
     *
     * @param int $maxSize
     * @return Group
     */
    public function setMaxSize($maxSize)
    {
        Arrays::setValueByPath($this->config, 'config.max_size', (int) $maxSize);
        return $this;
    }

    /**
     * Path to folder under PATH_site in which the files are stored. Example: 'uploads' or 'uploads/pictures'
     * [internal_type = *file* ONLY]
     *
     * @param string $uploadfolder
     * @return Group
     */
    public function setUploadfolder($uploadfolder)
    {
        Arrays::setValueByPath($this->config, 'config.uploadfolder', (string) $uploadfolder);
        return $this;
    }

    /**
     * Will prepend the table name to the stored relations (so instead of storing "23" you will store e.g. "tt_content_23").
     * [internal_type = *db* ONLY]
     *
     * @param bool $enable
     * @return Group
     */
    public function setPrependTname($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.prepend_tname', (bool) $enable);
        return $this;
    }

    /**
     * A list of tables which should not be remapped to the new element uids if the field holds elements that are copied
     * in the session. [internal_type = *db* ONLY]
     *
     * @param string $dontRemapTablesOnCopy list of tables
     * @return Group
     */
    public function setDontRemapTablesOnCopy($dontRemapTablesOnCopy)
    {
        Arrays::setValueByPath($this->config, 'config.dontRemapTablesOnCopy', (string) $dontRemapTablesOnCopy);
        return $this;
    }

    /**
     * Show thumbnails for the field in the TCEform.
     *
     * @param bool $enable
     * @return Group
     */
    public function setShowThumbs($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.show_thumbs', (bool) $enable);
        return $this;
    }

    /**
     * Height of the selector box in TCEforms. Since TYPO3 CMS 6.1, the default size is 5.
     *
     * @param int $size
     * @return Group
     */
    public function setSize($size)
    {
        Arrays::setValueByPath($this->config, 'config.size', (int) $size);
        return $this;
    }

    /**
     * If set, then the height of element listing selector box will automatically be adjusted to the number of selected
     * elements, however never less than "size" and never larger than the integer value of "autoSizeMax" itself
     * (takes precedence over "size"). So "autoSizeMax" is the maximum height the selector can ever reach.
     *
     * @param int $autoSizeMax
     * @return Group
     */
    public function setAutoSizeMax($autoSizeMax)
    {
        Arrays::setValueByPath($this->config, 'config.autoSizeMax', (int) $autoSizeMax);
        return $this;
    }

    /**
     * If set, this will override the default style of element selector box (which is "width:200px").
     *
     * @param string $selectedListStyle
     * @return Group
     */
    public function setSelectedListStyle($selectedListStyle)
    {
        Arrays::setValueByPath($this->config, 'config.selectedListStyle', (string) $selectedListStyle);
        return $this;
    }

    /**
     * Allows the same item more than once in a list.
     * If used with bidirectional MM relations it must be set for both the native and foreign field configuration.
     * Also, with MM relations in general you must use a UID field in the join table, see description for "MM".
     *
     * @param bool $enable
     * @return Group
     */
    public function setMultiple($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.multiple', (bool) $enable);
        return $this;
    }

    /**
     * @param int $maxitems
     * @return Group
     */
    public function setMaxitems($maxitems)
    {
        Arrays::setValueByPath($this->config, 'config.maxitems', (int) $maxitems);
        return $this;
    }

    /**
     * Minimum number of items in the selector box. (Default = 0)
     *
     * @param int $minitems
     * @return Group
     */
    public function setMinitems($minitems)
    {
        Arrays::setValueByPath($this->config, 'config.minitems', (int) $minitems);
        return $this;
    }

    /**
     * Removes the move icons next to the selector box.
     *
     * @param bool $enable
     * @return Group
     */
    public function setHideMoveIcons($enable = true)
    {
        Arrays::setValueByPath($this->config, 'config.hideMoveIcons', (bool) $enable);
        return $this;
    }

    /**
     * Disables sub-controls inside "group" control. Comma-separated list of values.
     *
     * @param string $disableControls
     * @return Group
     */
    public function setDisableControls($disableControls)
    {
        Arrays::setValueByPath($this->config, 'config.disable_controls', (string) $disableControls);
        return $this;
    }

    /**
     * Options for refining the appearance of group-type fields.
     * - elementBrowserType
     * - elementBrowserAllowed
     *
     * @param array $appearance
     * @return Group
     */
    public function setAppearance(array $appearance)
    {
        Arrays::setValueByPath($this->config, 'config.appearance', $appearance);
        return $this;
    }
}
