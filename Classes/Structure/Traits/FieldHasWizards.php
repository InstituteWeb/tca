<?php
namespace InstituteWeb\Tca\Structure\Traits;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * Trait ColumnHasWizard
 * Adds setWizards() and addWizard() method to column type.
 *
 * @package InstituteWeb\Tca
 * @see https://docs.typo3.org/typo3cms/TCAReference/AdditionalFeatures/WizardsConfiguration/Index.html#reserved-keys
 */
trait FieldHasWizards
{
    /**
     * @var \InstituteWeb\Tca\Structure\Wizards\AbstractWizard[]
     */
    protected $wizards = [];


    /**
     * Set wizard container options
     *
     * @param string|null $position
     * @param bool|null $vertical
     * @param int|null $distance
     * @param int|null $padding
     * @param string|null $valign
     * @return \InstituteWeb\Tca\Structure\Fields\AbstractField
     */
    public function setWizardContainerOptions($position = null, $vertical = null, $distance = null, $padding = null, $valign = null)
    {
        if (!is_array($this->config['wizards'])) {
            Arrays::setValueByPath($this->config, 'config.wizards', []);
        }

        if (!is_null($position)) {
            Arrays::setValueByPath($this->config, 'config.wizards._POSITION', (string) $position);
        }
        if (!is_null($vertical)) {
            Arrays::setValueByPath($this->config, 'config.wizards._VERTICAL', (bool) $vertical);
        }
        if (!is_null($distance)) {
            Arrays::setValueByPath($this->config, 'config.wizards._DISTANCE', (int) $distance);
        }
        if (!is_null($padding)) {
            Arrays::setValueByPath($this->config, 'config.wizards._PADDING', (int) $padding);
        }
        if (!is_null($valign)) {
            Arrays::setValueByPath($this->config, 'config.wizards._VALIGN', (string) $valign);
        }
        return $this;
    }

    /**
     * Add wizard to TCA column
     *
     * @param \InstituteWeb\Tca\Structure\Wizards\AbstractWizard $wizard
     * @return \InstituteWeb\Tca\Structure\Fields\AbstractField
     * @internal param array $configuration
     */
    public function addWizard(\InstituteWeb\Tca\Structure\Wizards\AbstractWizard $wizard)
    {
        $this->wizards[$wizard->getKey()] = $wizard;
        return $this;
    }

    /**
     * toArray() preprocess for applying wizards to field configuration
     * used in \InstituteWeb\Tca\Structure\Traits\ContainsConfigurations::toArray
     *
     * Wizards can update values of its parent. But just if the values are not already set.
     *
     * @param array $config
     * @return array
     */
    public function _toArrayPreprocess(array $config)
    {
        if (!is_array($config['wizards'])) {
            $config['wizards'] = [];
        }
        foreach ($this->wizards as $wizard) {
            if ($wizard->hasParentConfig()) {
                $config = array_merge($wizard->getParentConfig(), $config);
            }
            $config['wizards'][$wizard->getKey()] = $wizard->toArray();
        }
        if (empty($config['wizards'])) {
            unset($config['wizards']);
        }
        return $config;
    }
}
