<?php
namespace InstituteWeb\Tca\Structure\Traits;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

/**
 * Trait ContainsConfigurations
 * Adds config property and _set() and _get() method.
 *
 * @package InstituteWeb\Tca
 */
trait ContainsConfigurations
{
    /**
     * @var callable[]
     */
    protected $_toArrayPreprocessors = [];

    /**
     * Get value of configuration
     *
     * @param string $attribute Can also be array path
     * @return mixed
     */
    public function _get($attribute)
    {
        return \InstituteWeb\Tca\Utility\Arrays::getValueByPath($this->config, $attribute);
    }

    /**
     * Set value of configuration.
     * You can use paths in attribute like 'module.name' which addresses ['module']['name']
     *
     * @param string $attribute
     * @param mixed $value
     * @return \InstituteWeb\Tca\Structure\Fields\AbstractField
     * @internal
     */
    public function _set($attribute, $value)
    {
        \InstituteWeb\Tca\Utility\Arrays::setValueByPath($this->config, $attribute, $value);
        return $this;
    }

    /**
     * Returns $this->config array. You can pass a user function which should process the config are before.
     *
     * @return array
     */
    public function toArray()
    {
        if (method_exists($this, '_toArrayPreprocess')) {
            $returnValue = $this->_toArrayPreprocess($this->config);
            if (is_array($returnValue)) {
                $this->config = $returnValue;
            }
        }
        unset($this->config['_toArrayPreprocessors']);
        return $this->config;
    }

}
