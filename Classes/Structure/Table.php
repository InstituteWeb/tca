<?php
namespace InstituteWeb\Tca\Structure;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Structure;
use InstituteWeb\Tca\Structure\Fields\AbstractField;

/**
 * Class Table
 * This is the root entry of TCA structure. "tt_content" or "pages" are some popular examples.
 *
 * @package InstituteWeb\Tca
 */
class Table
{
    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var array|Ctrl assoc array with ctrl section keys (like label, label_alt, etc.) or ctrl object
     */
    protected $ctrl = [];

    /**
     * @var AbstractField[]
     */
    protected $fields = [];

    /**
     * @var array assoc array with interface keys (like showRecordFieldList, maxDBListItems, etc.)
     */
    protected $interface = ['showRecordFieldList' => ''];

    /**
     * @var Structure\Palette[]
     */
    protected $palettes = [];

    /**
     * @var Structure\Type[]
     */
    protected $types = [];


    /**
     * TcaTable constructor
     *
     * @param string $tableName e.g. 'tx_extension_domain_model_item'
     * @return Table
     */
    public function __construct($tableName)
    {
        $this->name = $tableName;
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getCtrl()
    {
        return $this->ctrl;
    }

    /**
     * @param array|Ctrl $ctrl
     * @return Table
     */
    public function setCtrl($ctrl)
    {
        $this->ctrl = $ctrl;
        return $this;
    }

    /**
     * @param array $interface
     * @return Table
     */
    public function setInterface(array $interface)
    {
        $this->interface = $interface;
        return $this;
    }

    /**
     * Get Fields
     *
     * @return Fields\AbstractField[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param AbstractField $field
     * @param bool|array $addToTypes "True" adds this column to all types. [ 0, 1 ] adds this column to type 0 and 1
     * @param bool $addToShowRecordFieldList When true the column is added to showRecordFieldList in interface section
     * @return Table
     */
    public function addField(AbstractField $field, $addToTypes = true, $addToShowRecordFieldList = true)
    {
        if (array_key_exists($field->getKey(), $this->fields)) {
            throw new \RuntimeException('The field "' . $field->getKey() . '" already exists in TCA of "' . $this->name . '"!');
        }

        $this->fields[$field->getKey()] = $field;

        if ($addToShowRecordFieldList) {
            $showRecordFieldList = explode(',', $this->interface['showRecordFieldList']);
            $showRecordFieldList[] = $field->getKey();
            $this->interface['showRecordFieldList'] = implode(',', array_filter($showRecordFieldList));
        }

        if ($addToTypes && empty($this->types)) {
            throw new \RuntimeException('Please define one type before you add columns to types');
        }
        if ($addToTypes === true) {
            foreach ($this->types as $type) {
                $type->addShowitem($field->getKey());
            }
        } else if (is_array($addToTypes)) {
            foreach ($this->types as $type) {
                if (in_array($type->getKey(), $addToTypes)) {
                    $type->addShowitem($field->getKey());
                }
            }
        }

        return $this;
    }

    /**
     * @param Structure\Type $type
     * @return Table
     */
    public function addType(Structure\Type $type)
    {
        if (array_key_exists($type->getKey(), $this->types)) {
            throw new \RuntimeException('The type "' . $type->getKey() . '" already exists in TCA of "' . $this->name . '"!');
        }
        $this->types[$type->getKey()] = $type;

        return $this;
    }

    /**
     * @param Structure\Palette $palette
     * @return Table
     */
    public function addPalette(Structure\Palette $palette)
    {
        if (array_key_exists($palette->getKey(), $this->palettes)) {
            throw new \RuntimeException('The palette "' . $palette->getKey() . '" already exists in TCA of "' . $this->name . '"!');
        }
        $this->palettes[$palette->getKey()] = $palette;

        return $this;
    }

    /**
     * @return array
     */
    protected function getFieldsAsArray()
    {
        $fields = [];
        /** @var AbstractField $field */
        foreach ($this->fields as $field) {
            $fields[$field->getKey()] = $field->toArray();
        }
        return $fields;
    }

    /**
     * @return array
     */
    protected function getTypesAsArray()
    {
        $types = [];
        /** @var Structure\Type $type */
        foreach ($this->types as $type) {
            $types[$type->getKey()] = $type->toArray();
        }
        ksort($types);
        return $types;
    }

    /**
     * @return array
     */
    protected function getPalettesAsArray()
    {
        $palettes = [];
        foreach ($this->palettes as $palette) {
            $palettes[$palette->getKey()] = $palette->toArray();
        }
        return $palettes;
    }

    /**
     * Returns the actual TCA
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'ctrl' => is_array($this->ctrl) ? $this->ctrl : $this->ctrl->toArray(),
            'interface' => $this->interface,
            'types' => $this->getTypesAsArray(),
            'palettes' => $this->getPalettesAsArray(),
            'columns' => $this->getFieldsAsArray(),
        ];
    }
}
