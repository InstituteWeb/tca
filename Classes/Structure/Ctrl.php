<?php
namespace InstituteWeb\Tca\Structure;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * Ctrl class
 *
 * @package InstituteWeb\Tca
 */
class Ctrl
{
    use \InstituteWeb\Tca\Structure\Traits\ContainsConfigurations;

    /**
     * @var array
     */
    protected $config = [
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'enablecolumns' => [
            'fe_group' => 'fe_group',
        ],
    ];

    /**
     * Ctrl constructor
     *
     * @param bool $hidden
     * @param bool $startEndTime
     * @param bool $deleted
     * @param bool $sorting
     * @param bool $translations
     * @param bool $workspaces
     * @return Ctrl
     */
    public function __construct(
        $hidden = true,
        $startEndTime = false,
        $deleted = true,
        $sorting = true,
        $translations = false,
        $workspaces = false
    ) {

        if ($hidden) {
            Arrays::setValueByPath($this->config, 'enablecolumns.disabled', 'hidden');
        }
        if ($startEndTime) {
            Arrays::setValueByPath($this->config, 'enablecolumns.starttime', 'starttime');
            Arrays::setValueByPath($this->config, 'enablecolumns.endtime', 'endtime');
        }
        if ($deleted) {
            Arrays::setValueByPath($this->config, 'deleted', 'deleted');
        }
        if ($sorting) {
            Arrays::setValueByPath($this->config, 'sortby', 'sorting');
        }
        if ($translations) {
            Arrays::setValueByPath($this->config, 'languageField', 'sys_language_uid');
            Arrays::setValueByPath($this->config, 'transOrigPointerField', 'l10n_parent');
            Arrays::setValueByPath($this->config, 'transOrigDiffSourceField', 'l10n_diffsource');
        }
        if ($workspaces) {
            Arrays::setValueByPath($this->config, 'versioningWS', true);
        }
    }

    /**
     * Contains the system name of the table. Is used for display in the backend.
     *
     * @param string $title or LLL reference
     * @return Ctrl
     */
    public function setTitle($title)
    {
        Arrays::setValueByPath($this->config, 'title', (string) $title);
        return $this;
    }

    /**
     * Points to the field name of the table which should be used as the "title" when the record is displayed in the system.
     *
     * @param string $label field name
     * @return Ctrl
     */
    public function setLabel($label)
    {
        Arrays::setValueByPath($this->config, 'label', (string) $label);
        return $this;
    }

    /**
     * Comma-separated list of field names, which are holding alternative values to the value from the field pointed to
     * by "label" (see above) if that value is empty. May not be used consistently in the system, but should apply in
     * most cases.
     *
     * @param string $labelAlt comma-separated list of field names
     * @return Ctrl
     */
    public function setLabelAlt($labelAlt)
    {
        Arrays::setValueByPath($this->config, 'label_alt', (string) $labelAlt);
        return $this;
    }

    /**
     * If set, then the label_alt fields are always shown in the title separated by comma.
     *
     * @param bool $enable
     * @return Ctrl
     */
    public function setLabelAltForce($enable = true)
    {
        Arrays::setValueByPath($this->config, 'label_alt_force', (bool) $enable);
        return $this;
    }

    /**
     * Function or method reference. This can be used whenever the label or label_alt options don't offer enough flexibility,
     * e.g. when you want to look up another table to create your label. The result of this function overrules the label,
     * label_alt or label_alt_force settings.
     *
     * @param string $labelUserFunc
     * @return Ctrl
     */
    public function setLabelUserFunc($labelUserFunc)
    {
        Arrays::setValueByPath($this->config, 'label_userFunc', (string) $labelUserFunc);
        return $this;
    }

    /**
     * Options for label_userFunc. The array of options is passed to the user function in the parameters array with
     * key "options".
     *
     * @param string $labelUserFuncOptions
     * @return Ctrl
     */
    public function setLabelUserFuncOptions($labelUserFuncOptions)
    {
        Arrays::setValueByPath($this->config, 'label_userFunc_options', (string) $labelUserFuncOptions);
        return $this;
    }

    /**
     * Similar to label_userFunc but allowed to return formatted HTML for the label and used only for the labels of
     * inline (IRRE) records. The referenced user function may receive optional arguments using the
     * formattedLabel_userFunc_options property.
     *
     * @param string $formattedLabelUserFunc
     * @return Ctrl
     */
    public function setFormattedLabelUserFunc($formattedLabelUserFunc)
    {
        Arrays::setValueByPath($this->config, 'formattedLabel_userFunc', (string) $formattedLabelUserFunc);
        return $this;
    }

    /**
     * Options for formattedLabel_userFunc.
     *
     * @param string $formattedLabelUserFuncOptions
     * @return Ctrl
     */
    public function setFormattedLabelUserFuncOptions($formattedLabelUserFuncOptions)
    {
        Arrays::setValueByPath($this->config, 'formattedLabel_userFunc_options', (string) $formattedLabelUserFuncOptions);
        return $this;
    }

    /**
     * Field name, which defines the "record type".
     * The value of this field determines which one of the 'types' configurations are used for displaying the fields in
     * the TCEforms. It will probably also affect how the record is used in the context where it belongs.
     *
     * @param string $type field name
     * @return Ctrl
     */
    public function setType($type)
    {
        Arrays::setValueByPath($this->config, 'type', (string) $type);
        return $this;
    }

    /**
     * Hide this table in record listings.
     *
     * @param bool $enable
     * @return Ctrl
     */
    public function setHideTable($enable = true)
    {
        Arrays::setValueByPath($this->config, 'hideTable', $enable);
        return $this;
    }

    /**
     * This is a list of fields that will trigger an update of the form, on top of the "type" field. This is generally
     * done to hide or show yet more fields depending on the value of the field that triggered the update.
     *
     * @param string $requestUpdate list of field names
     * @return Ctrl
     */
    public function setRequestUpdate($requestUpdate)
    {
        Arrays::setValueByPath($this->config, 'requestUpdate', (string) $requestUpdate);
        return $this;
    }

    /**
     * Pointing to the icon file to use for the table.
     * Icons should be square SVGs. In case you cannot supply a SVG you can still use a PNG file of 64x64 pixels in dimension.
     *
     * @param string $iconfile
     * @return Ctrl
     */
    public function setIconfile($iconfile)
    {
        Arrays::setValueByPath($this->config, 'iconfile', (string) $iconfile);
        return $this;
    }

    /**
     * Field name, whose value decides alternative icons for the table records (The default icon is the one defined with
     * the 'iconfile' value.) The values in the field referenced by this property must match entries in the array defined
     * in typeicon_classes properties. If no match is found, the default icon is used.
     *
     * @param string $typeiconColumn field name
     * @return Ctrl
     */
    public function setTypeiconColumn($typeiconColumn)
    {
        Arrays::setValueByPath($this->config, 'typeicon_column', (string) $typeiconColumn);
        return $this;
    }

    /**
     * Array of class names to use for the records. The keys must correspond to the values found in the column
     * referenced in the typeicon_column property. The class names correspond to the backend's sprite icons.
     *
     * Tip: To register your own icons with the global backend sprite, use method
     * \TYPO3\CMS\Backend\Sprite\SpriteManager::addSingleIcons().
     *
     * @param array $typeiconClasses
     * @return Ctrl
     */
    public function setTypeiconClasses(array $typeiconClasses)
    {
        Arrays::setValueByPath($this->config, 'typeicon_classes', $typeiconClasses);
        return $this;
    }

    /**
     * Field name, which contains the value for any thumbnails of the records.
     * This could be a field of the "group" type containing a list of file names.
     *
     * @param string $thumbnail field name
     * @return Ctrl
     */
    public function setThumbnail($thumbnail)
    {
        Arrays::setValueByPath($this->config, 'thumbnail', (string) $thumbnail);
        return $this;
    }

    /**
     * Field name, which contains the thumbnail image used to represent the record visually whenever it is shown in
     * TCEforms as a foreign reference selectable from a selector box.
     *
     * @param string $seliconField field name
     * @return Ctrl
     */
    public function setSeliconField($seliconField)
    {
        Arrays::setValueByPath($this->config, 'selicon_field', (string) $seliconField);
        return $this;
    }

    /**
     * The path prefix of the value from selicon_field. This must the same as the "upload_path" of that field.
     *
     * @param string $seliconFieldPath
     * @return Ctrl
     */
    public function setSeliconFieldPath($seliconFieldPath)
    {
        Arrays::setValueByPath($this->config, 'selicon_field_path', (string) $seliconFieldPath);
        return $this;
    }

    /**
     * Field name, which is used to manage the order of the records.
     * The field will contain an integer value which positions it at the correct position between other records from the
     * same table on the current page.
     *
     * @param string $sortby field name
     * @return Ctrl
     */
    public function setSortby($sortby)
    {
        Arrays::setValueByPath($this->config, 'sortby', (string) $sortby);
        return $this;
    }

    /**
     * If a field name for sortby is defined, then this is ignored.
     * Otherwise this is used as the 'ORDER BY' statement to sort the records in the table when listed in the TYPO3 backend.
     *
     * @param string $defaultSortby
     * @return Ctrl
     */
    public function setDefaultSortby($defaultSortby)
    {
        Arrays::setValueByPath($this->config, 'default_sortby', (string) $defaultSortby);
        return $this;
    }

    /**
     * Points to the palette-number(s) or identifiers that should always be shown in the bottom of the TCEform.
     *
     * @param string $mainpalette
     * @return Ctrl
     */
    public function setMainpalette($mainpalette)
    {
        Arrays::setValueByPath($this->config, 'mainpalette', (string) $mainpalette);
        return $this;
    }

    /**
     * Field name, which is automatically updated to the current timestamp (UNIX-time in seconds) each time the record
     * is updated/saved in the system. Typically the name "tstamp" is used for that field.
     *
     * @param string $tstamp field name
     * @return Ctrl
     */
    public function setTstamp($tstamp)
    {
        Arrays::setValueByPath($this->config, 'tstamp', (string) $tstamp);
        return $this;
    }

    /**
     * Field name, which is automatically set to the current timestamp when the record is created. Is never modified again.
     * Typically the name "crdate" is used for that field.
     *
     * @param string $crdate field name
     * @return Ctrl
     */
    public function setCrdate($crdate)
    {
        Arrays::setValueByPath($this->config, 'crdate', (string) $crdate);
        return $this;
    }

    /**
     * Field name, which is automatically set to the uid of the backend user (be_users) who originally created the record.
     * Is never modified again. Typically the name "cruser_id" is used for that field.
     *
     * @param string $cruserId field name
     * @return Ctrl
     */
    public function setCruserId($cruserId)
    {
        Arrays::setValueByPath($this->config, 'cruser_id', (string) $cruserId);
        return $this;
    }

    /**
     * Determines where a record may exist in the page tree.
     *
     * There are three options depending:
     * - 0 (false): Can only exist in the page tree.
     * - 1 (true): Can only exist in the root.
     * - -1: Can exist in both page tree and root.
     *
     * @param int $rootLevel values: 0, 1, -1
     * @return Ctrl
     */
    public function setRootLevel($rootLevel)
    {
        Arrays::setValueByPath($this->config, 'rootLevel', (string) $rootLevel);
        return $this;
    }

    /**
     * Records from this table may not be edited in the TYPO3 backend. Such tables are usually called "static".
     *
     * @param bool $enable
     * @return Ctrl
     */
    public function setReadOnly($enable = true)
    {
        Arrays::setValueByPath($this->config, 'readOnly', (string) $enable);
        return $this;
    }

    /**
     * Records may be changed only by "admin"-users (having the "admin" flag set).
     *
     * @param bool $enable
     * @return Ctrl
     */
    public function setAdminOnly($enable = true)
    {
        Arrays::setValueByPath($this->config, 'adminOnly', (string) $enable);
        return $this;
    }

    /**
     * Field name, which – if set – will prevent all editing of the record for non-admin users.
     *
     * The field should be configured as a checkbox type. Non-admins could be allowed to edit the checkbox but if they
     * set it, they will effectively lock the record so they cannot edit it again – and they need an Admin- user to
     * remove the lock.
     *
     * Note that this flag is cleared when a new copy or version of the record is created.
     * This feature is used on the pages table, where it also prevents editing of records on that page (except other pages)!
     * Also, no new records (including pages) can be created on the page.
     *
     * @param string $editlock field name
     * @return Ctrl
     */
    public function setEditLock($editlock)
    {
        Arrays::setValueByPath($this->config, 'editlock', (string) $editlock);
        return $this;
    }

    /**
     * Field name, which will contain the UID of the original record in case a record is created as a copy or new
     * version of another record.
     *
     * Is used when new versions are created from elements and enables the backend to display a visual comparison between
     * a new version and its original.
     *
     * @param string $origUid field name
     * @return Ctrl
     */
    public function setOrigUid($origUid)
    {
        Arrays::setValueByPath($this->config, 'origUid', (string) $origUid);
        return $this;
    }

    /**
     * Field name, which indicates if a record is considered deleted or not.
     * If this feature is used, then records are not really deleted, but just marked 'deleted' by setting the value of
     * the field name to "1". And in turn the whole system must strictly respect the record as deleted. This means that
     * any SQL query must exclude records where this field is true.
     *
     * This is a very common feature. Most tables use it throughout the TYPO3 Core.
     *
     * @param string $delete field name
     * @return Ctrl
     */
    public function setDelete($delete)
    {
        Arrays::setValueByPath($this->config, 'delete', (string) $delete);
        return $this;
    }

    /**
     * Field name where description of a record is stored in.
     * This description is only displayed in the backend to guide editors and admins.
     *
     * @param string $descriptionColumn field name
     * @return Ctrl
     */
    public function setDescriptionColumn($descriptionColumn)
    {
        Arrays::setValueByPath($this->config, 'descriptionColumn', (string) $descriptionColumn);
        return $this;
    }

    /**
     * Specifies which publishing control features are automatically implemented for the table.
     *
     * @param array $enablecolumns
     * @return Ctrl
     */
    public function setEnableColumns(array $enablecolumns)
    {
        Arrays::setValueByPath($this->config, 'enablecolumns', $enablecolumns);
        return $this;
    }

    /**
     * Comma-separated list of fields from the table that will be included when searching for records in the TYPO3
     * backend. Starting with TYPO3 CMS 4.6, no record from a table will ever be found if that table does not have "searchFields" defined.
     *
     * There are finer controls per column, see the "search" property in the list of "Common properties" further in this
     * manual.
     *
     * @param string $searchFields
     * @return Ctrl
     */
    public function setSearchFields($searchFields)
    {
        Arrays::setValueByPath($this->config, 'searchFields', (string) $searchFields);
        return $this;
    }

    /**
     * This option can be used to group records in the new record wizard. If you define a new table and set its
     * "groupName" to the key of another extension, your table will appear in the list of records from that other
     * extension in the new record wizard.
     *
     * @param string $groupName
     * @return Ctrl
     */
    public function setGroupName($groupName)
    {
        Arrays::setValueByPath($this->config, 'groupName', (string) $groupName);
        return $this;
    }

    /**
     * If set, and the "disabled" field from enablecolumns is specified, then records will be disabled/hidden when they
     * are copied.
     *
     * @param bool $enable
     * @return Ctrl
     */
    public function setHideAtCopy($enable = true)
    {
        Arrays::setValueByPath($this->config, 'hideAtCopy', (bool) $enable);
        return $this;
    }

    /**
     * This string will be prepended the records title field when the record is inserted on the same PID as the original
     * record (thus you can distinguish them).
     *
     * Usually the value is something like " (copy %s)" which tells that it was a copy that was just inserted
     * (The token "%s" will take the copy number).
     *
     * @param string $prependAtCopy or LLL reference
     * @return Ctrl
     */
    public function setPrependAtCopy($prependAtCopy)
    {
        Arrays::setValueByPath($this->config, 'prependAtCopy', (string) $prependAtCopy);
        return $this;
    }

    /**
     * The fields in this list will automatically have the value of the same field from the "previous" record
     * transferred when they are copied or moved to the position after another record from same table.
     *
     * @param string $copyAfterDuplFields list of field names
     * @return Ctrl
     */
    public function setCopyAfterDuplFields($copyAfterDuplFields)
    {
        Arrays::setValueByPath($this->config, 'copyAfterDuplFields', (string) $copyAfterDuplFields);
        return $this;
    }

    /**
     * These fields are restored to the default value of the record when they are copied.
     *
     * @param string $setToDefaultOnCopy list of field names
     * @return Ctrl
     */
    public function setSetToDefaultOnCopy($setToDefaultOnCopy)
    {
        Arrays::setValueByPath($this->config, 'setToDefaultOnCopy', (string) $setToDefaultOnCopy);
        return $this;
    }

    /**
     * When a new record is created, this defines the fields from the 'previous' record that should be used as default
     * values.
     *
     * @param string $useColumnsForDefaultValues list of field names
     * @return Ctrl
     */
    public function setUseColumnsForDefaultValues($useColumnsForDefaultValues)
    {
        Arrays::setValueByPath($this->config, 'useColumnsForDefaultValues', (string) $useColumnsForDefaultValues);
        return $this;
    }

    /**
     * When a new element is created in a draft workspace a placeholder element is created in the Live workspace.
     * Some values must be stored in this placeholder and not just in the overlay record. A typical example would be
     * sys_language_uid. This property defines the list of fields whose values are "shadowed" to the Live record.
     *
     * All fields listed for this option must be defined in $TCA[<table>]['columns'] as well.
     * Furthermore fields which are listed in transOrigPointerField, languageField, label and type are automatically
     * added to this list of fields and do not have to be mentioned again here.
     *
     * @param string $shadowColumnsForNewPlaceholders list of field names
     * @return Ctrl
     */
    public function setShadowColumnsForNewPlaceholders($shadowColumnsForNewPlaceholders)
    {
        Arrays::setValueByPath($this->config, 'shadowColumnsForNewPlaceholders', (string) $shadowColumnsForNewPlaceholders);
        return $this;
    }

    /**
     * Similar to shadowColumnsForNewPlaceholders but for move placeholders. It is used when:
     * - changing the sorting order of elements on the same page
     * - moving elements to a different page
     *
     * @param string $shadowColumnsForMovePlaceholders list of field names
     * @return Ctrl
     */
    public function setShadowColumnsForMovePlaceholders($shadowColumnsForMovePlaceholders)
    {
        Arrays::setValueByPath($this->config, 'shadowColumnsForMovePlaceholders', (string) $shadowColumnsForMovePlaceholders);
        return $this;
    }

    /**
     * This marks a table to be "static".
     * A "static table" means that it should not be updated for individual databases because it is meant to be centrally
     * updated and distributed. For instance static tables could contain country-codes used in many systems.
     *
     * The foremost property of a static table is that the uid's used are the SAME across systems.
     * Import/Export of records expect static records to be common for two systems.
     *
     * @param bool $enable
     * @return Ctrl
     */
    public function setIsStatic($enable = true)
    {
        Arrays::setValueByPath($this->config, 'is_static', (bool) $enable);
        return $this;
    }

    /**
     * Field name which is used to store the uid of a frontend user if the record is created through fe_adminLib.
     *
     * @param string $feCruserId field name
     * @return Ctrl
     */
    public function setFeCruserId($feCruserId)
    {
        Arrays::setValueByPath($this->config, 'fe_cruser_id', (string) $feCruserId);
        return $this;
    }

    /**
     * Field name which is used for storing the uid of a frontend group whose members are allowed to edit through
     * fe_adminLib.
     *
     * @param string $feCrgroupId field name
     * @return Ctrl
     */
    public function setFeCrgroupId($feCrgroupId)
    {
        Arrays::setValueByPath($this->config, 'fe_crgroup_id', (string) $feCrgroupId);
        return $this;
    }

    /**
     * Field name which points to the field name which - as a boolean - will prevent any editing by the fe_adminLib if
     * set. Say if the "fe_cruser_id" field matches the current fe_user normally the field is editable. But with this
     * option, you could make a check-box in the backend that would lock this option.
     *
     * @param string $feAdminLock field name
     * @return Ctrl
     */
    public function setFeAdminLock($feAdminLock)
    {
        Arrays::setValueByPath($this->config, 'fe_admin_lock', (string) $feAdminLock);
        return $this;
    }

    /**
     * Localization access control.
     * Field name which contains the pointer to the language of the record's content. Language for a record is defined
     * by an integer pointing to a "sys_language" record (found in the page tree root).
     *
     * @param string $languageField field name
     * @return Ctrl
     */
    public function setLanguageField($languageField)
    {
        Arrays::setValueByPath($this->config, 'languageField', (string) $languageField);
        return $this;
    }

    /**
     * Name of the field used by translations to point back to the original record (i.e. the record in the default
     * language of which they are a translation).
     *
     * @param string $transOrigPointerField field name
     * @return Ctrl
     */
    public function setTransOrigPointerField($transOrigPointerField)
    {
        Arrays::setValueByPath($this->config, 'transOrigPointerField', (string) $transOrigPointerField);
        return $this;
    }

    /**
     * Translations may be stored in a separate table, instead of the same one. In such a case, the name of the
     * translation table is stored in this property. The translation table in turn will use the transOrigPointerTable
     * property to point back to this table.
     *
     * @param string $transForeignTable
     * @return Ctrl
     */
    public function setTransForeignTable($transForeignTable)
    {
        Arrays::setValueByPath($this->config, 'transForeignTable', (string) $transForeignTable);
        return $this;
    }

    /**
     * Symmetrical property to "transForeignTable".
     *
     * @param string $transOrigPointerTable table name
     * @return Ctrl
     */
    public function setTransOrigPointerTable($transOrigPointerTable)
    {
        Arrays::setValueByPath($this->config, 'transOrigPointerTable', (string) $transOrigPointerTable);
        return $this;
    }

    /**
     * Field name which will be updated with the value of the original language record whenever the translation record
     * is updated. This information is later used to compare the current values of the default record with those stored
     * in this field and if they differ there will be a display in the form of the difference visually.
     *
     * This is a big help for translators so they can quickly grasp the changes that happened to the
     * default language text.
     *
     * @param string $transOrigDiffSourceField field name
     * @return Ctrl
     */
    public function setTransOrigDiffSourceField($transOrigDiffSourceField)
    {
        Arrays::setValueByPath($this->config, 'transOrigDiffSourceField', (string) $transOrigDiffSourceField);
        return $this;
    }

    /**
     * If set, versioning is enabled for this table. If integer it indicates a version number of versioning features.
     * Version 2: Support for moving elements was added. ("V2" is used to mark features)
     *
     * @param bool|string $versioningWs
     * @return Ctrl
     */
    public function setversioningWs($versioningWs)
    {
        Arrays::setValueByPath($this->config, 'versioningWS', $versioningWs);
        return $this;
    }

    /**
     * If set, this table can always be edited live even in a workspace and even if "live editing" is not enabled in a
     * custom workspace. For instance this is set by default for Backend user and group records since it is assumed that
     * administrators like the flexibility of editing backend users without having to go to the Live workspace.
     *
     * @param bool $enable
     * @return Ctrl
     */
    public function setVersioningWsAlwaysAllowLiveEdit($enable)
    {
        Arrays::setValueByPath($this->config, 'versioningWS_alwaysAllowLiveEdit', (bool) $enable);
        return $this;
    }

    /**
     * If set, content from this table will get copied along when a new version of a page is created.
     * (Only for other tables than "pages")
     *
     * @param bool $enable
     * @return Ctrl
     */
    public function setVersioningFollowPages($enable = true)
    {
        Arrays::setValueByPath($this->config, 'versioning_followPages', (bool) $enable);
        return $this;
    }

    /**
     * Array of sub-properties, see Security-related configuration.
     *
     * @param array $security
     * @return Ctrl
     */
    public function setSecurity(array $security)
    {
        Arrays::setValueByPath($this->config, 'security', $security);
        return $this;
    }

    /**
     * User-defined content for extensions. You can use this as you like.
     *
     * @param array $ext
     * @return Ctrl
     */
    public function setExt(array $ext)
    {
        Arrays::setValueByPath($this->config, 'EXT', $ext);
        return $this;
    }

    /**
     * Allows users to access records that are not in their defined web-mount, thus bypassing this restriction.
     *
     * @param bool $enable
     * @return Ctrl
     */
    public function setIgnoreWebMountRestriction($enable = true)
    {
        Arrays::setValueByPath($this->config, 'ignoreWebMountRestriction', (string) $enable);
        return $this;
    }

    /**
     * Allows non-admin users to access records that on the root-level (page-id 0), thus bypassing this usual restriction.
     *
     * @param bool $enable
     * @return Ctrl
     */
    public function setIgnoreRootLevelRestriction($enable = true)
    {
        Arrays::setValueByPath($this->config, 'ignoreRootLevelRestriction', (bool) $enable);
        return $this;
    }
}
