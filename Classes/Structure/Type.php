<?php
namespace InstituteWeb\Tca\Structure;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * Type class
 *
 * @package InstituteWeb\Tca
 */
class Type
{
    use \InstituteWeb\Tca\Structure\Traits\ContainsConfigurations;

    /**
     * @var int Numeric key of this type (starting with 0)
     */
    protected $key = 0;

    /**
     * @var array
     */
    protected $config = [
        'showitem' => '',
    ];

    /**
     * Type constructor.
     *
     * @param int $key
     * @param string $showitem
     * @return Type
     */
    public function __construct($key = 0, $showitem = '')
    {
        $this->key = (int) $key;
        $this->setShowitem($showitem);
    }

    /**
     * @return int
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $showitem
     * @return Type
     */
    public function setShowitem($showitem)
    {
        Arrays::setValueByPath($this->config, 'showitem', (string) $showitem);
        return $this;
    }

    /**
     * @param string $singleShowitem
     * @return Type
     */
    public function addShowitem($singleShowitem)
    {
        $showitem = explode(',', $this->config['showitem']);
        $showitem[] = $singleShowitem;
        Arrays::setValueByPath($this->config, 'showitem', implode(',', array_filter($showitem)));
        return $this;
    }

    /**
     * @param array $columnOverrides
     * @return Type
     */
    public function setColumnOverrides(array $columnOverrides)
    {
        Arrays::setValueByPath($this->config, 'columnOverrides', $columnOverrides);
        return $this;
    }

    /**
     * @param string $subtype_value_field
     * @return Type
     */
    public function setSubtypeValueField($subtype_value_field)
    {
        Arrays::setValueByPath($this->config, 'subtype_value_field', (string) $subtype_value_field);
        return $this;
    }

    /**
     * @param array $subtypes_excludelist
     * @return Type
     */
    public function setSubtypesExcludelist(array $subtypes_excludelist)
    {
        Arrays::setValueByPath($this->config, 'subtypes_excludelist', $subtypes_excludelist);
        return $this;
    }

    /**
     * @param array $subtypes_addlist
     * @return Type
     */
    public function setSubtypesAddlist(array $subtypes_addlist)
    {
        Arrays::setValueByPath($this->config, 'subtypes_addlist', $subtypes_addlist);
        return $this;
    }

    /**
     * @param string $bitmask_value_field
     * @return Type
     */
    public function setBitmaskValueField($bitmask_value_field)
    {
        Arrays::setValueByPath($this->config, 'bitmask_value_field', (string) $bitmask_value_field);
        return $this;
    }

    /**
     * @param array $bitmask_excludelist_bits
     * @return Type
     */
    public function setBitmaskExcludelistBits(array $bitmask_excludelist_bits)
    {
        Arrays::setValueByPath($this->config, 'bitmask_excludelist_bits', $bitmask_excludelist_bits);
        return $this;
    }
}
