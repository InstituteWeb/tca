<?php
namespace InstituteWeb\Tca\Structure;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2016 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use InstituteWeb\Tca\Utility\Arrays;

/**
 * Palette class
 *
 * @package InstituteWeb\Tca
 */
class Palette
{
    use \InstituteWeb\Tca\Structure\Traits\ContainsConfigurations;

    /**
     * @var string key of this interface
     */
    protected $key;

    /**
     * @var string
     */
    protected $showitem;

    /**
     * @var bool
     */
    protected $isHiddenPalette = false;

    /**
     * @var array
     */
    protected $config = ['showitem' => ''];


    /**
     * Type constructor.
     *
     * @param int $key
     * @param string $showitem
     * @param bool $isHiddenPalette
     * @return Palette
     */
    public function __construct($key, $showitem = '', $isHiddenPalette = false)
    {
        $this->key = (string) $key;
        $this->setShowitem($showitem);
        if ($isHiddenPalette) {
            $this->setIsHiddenPalette($isHiddenPalette);
        }
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $showitem
     * @return Type
     */
    public function setShowitem($showitem)
    {
        Arrays::setValueByPath($this->config, 'showitem', (string) $showitem);
        return $this;
    }

    /**
     * @param string $singleShowitem
     * @return Type
     */
    public function addShowitem($singleShowitem)
    {
        $showitem = explode(',', $this->config['showitem']);
        $showitem[] = $singleShowitem;
        Arrays::setValueByPath($this->config, 'showitem', implode(',', array_filter($showitem)));
        return $this;
    }

    /**
     * @param boolean $enable
     * @return Type
     */
    public function setIsHiddenPalette($enable = true)
    {
        Arrays::setValueByPath($this->config, 'isHiddenPalette', (bool) $enable);
        return $this;
    }
}
